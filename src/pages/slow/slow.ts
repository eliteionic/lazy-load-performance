import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-slow',
  templateUrl: 'slow.html',
})
export class SlowPage {

	cards: any[] = [];

	constructor(public navCtrl: NavController, public navParams: NavParams) {

	}

	ionViewDidLoad() {
		this.cards = new Array(1000);
	}

}
