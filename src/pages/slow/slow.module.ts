import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SlowPage } from './slow';

@NgModule({
  declarations: [
    SlowPage,
  ],
  imports: [
    IonicPageModule.forChild(SlowPage),
  ],
})
export class SlowPageModule {}
